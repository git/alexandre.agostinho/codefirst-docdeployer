# Docdeployer

A Drone CI tool to easily deploy documentation on CodeDoc

## Note
This repo is forked from <https://codefirst.iut.uca.fr/git/maxime.batista/codefirst-docdeployer>.

I only made a few small changes to this repository to adapt it to my way of working.

## Usage:
```yml
steps:
  - name: generate-and-deploy-docs
    image: hub.codefirst.iut.uca.fr/alexandre.agostinho/codefirst-docdeployer:latest
    failure: ignore
    volumes:
      - name: docs
        path: /docs
    commands:
      - /entrypoint.sh <arguments>
```

`entrypoint.sh`  
This command generates the documentation and deploys it on your CodeDoc space

### Arguments

- `-b --branch [branch]` pattern of branch names to deploy
- `-l --loc [dir]` location of the documentation sources root - **required**
- `-t --type [docusaurus|doxygen|mdbook|swagger]` type of documentation generator to use. - **required**
- `-d --dest [dir]` path to where to put the documentation outputs in your repository's CodeDoc space - **optional <u>!! cannot be absolute !!</u>**

### Examples :

```
/entrypoint.sh -l ./documentation/api/api-swagger.yml -t swagger -d api_documentation
/entrypoint.sh -l ./documentation/app -t docusaurus -d application_documentation
/entrypoint.sh -b main -l ./documentation/doxygen/ -t doxygen -d sourcecode_documentation
/entrypoint.sh -l ./doc -t mdbook -d book
```

---

For more informations, check the [original repository](https://codefirst.iut.uca.fr/git/maxime.batista/codefirst-docdeployer).
