GEN_PATH="/docs/swagger"

generate() {
  if [[ ! "$1" ]]; then
    echo "missing swagger config file ($1)" >&2
  fi

  mkdir -p $GEN_PATH

  cat <<EOF > $GEN_PATH/CLICKME.html
<!DOCTYPE html>
  <html>
    <head>
      <meta http-equiv="Refresh" content="0; url=/swagger?url=/documentation/${RELATIVE_PATH}swagger/$1" />
    </head>
    <body>
      <p>Suivez <a href="swagger?url=/swagger?url=/documentation/${RELATIVE_PATH}swagger/$1">ce lien</a>.</p>
    </body>
</html>
EOF

  mv "$1" $GEN_PATH
}
