GEN_PATH="/docs/book"

generate() {

  if [ ! -f "$1"/book.toml ]; then
    echo "mdbook generator error : file $1/book.toml is missing" >&2
    exit 1
  fi

  mkdir -p $GEN_PATH
  mdbook build "$1" -d $GEN_PATH
}
