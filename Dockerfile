FROM debian:bookworm
LABEL author="Maxime Batista"

RUN apt update && apt -y install sshpass rsync doxygen nodejs npm xdot xdotool wget

WORKDIR /usr/local
RUN npm install docusaurus

RUN wget -qO- 'https://github.com/rust-lang/mdBook/releases/download/v0.4.25/mdbook-v0.4.25-x86_64-unknown-linux-gnu.tar.gz' | tar xvz -C /usr/local/bin

RUN mkdir -p /root/.ssh
COPY ./id_rsa /root/.ssh/
RUN chmod 700 /root/.ssh && chmod 600 /root/.ssh/*

COPY entrypoint.sh /
COPY generators /generators
RUN chmod +x /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
