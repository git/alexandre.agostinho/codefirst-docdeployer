GEN_PATH="/docs/docusaurus"

generate() {
  (cd "$1" && npm install && npm run build)

  mkdir -p $GEN_PATH
  mv build/* $GEN_PATH
}
