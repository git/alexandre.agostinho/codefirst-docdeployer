GEN_PATH="/docs/doxygen"

generate() {

  if [ ! -f "$1"/Doxyfile ]; then
    echo "doxygen generator error : file $1/Doxyfile is missing" >&2
    exit 1
  fi

  mkdir -p $GEN_PATH
  cd "$1"
  (cat Doxyfile; echo -e "\nOUTPUT_DIRECTORY = \nHTML_OUTPUT = $GEN_PATH") | doxygen -
}
