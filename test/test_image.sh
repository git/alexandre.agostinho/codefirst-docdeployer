#!/bin/bash

set -eu

declare -g EXPECTED_DOC_BASE_PATH='/usr/share/nginx/html/octocat/hello-world'
declare -g VERBOSE='false'

root_dir=$(readlink -f $(dirname $(realpath "$0"))/..)
if ! [ -f "$root_dir"/id_rsa ]; then
  echo 'Generating SSH key'
  ssh-keygen -t rsa -f "$root_dir/id_rsa" -q -N ''
fi

run_image() {
  # Run the codefirst-docdeployer image with the given branch and command.
  #
  # The container will automatically be added to the same network as a container
  # named 'nginx'. The codefirst-nginx container acts as a SSH server and will
  # be started and stopped automatically. The SSH user, port and authorized
  # keys are already configured.
  local branch=$1
  local pubkey=$(<"$root_dir/id_rsa.pub")
  shift
  docker network inspect codefirst &> /dev/null || docker network create codefirst
  nginx_id=$(docker run --rm -d -v codefirst-nginx:/usr/share/nginx --net=codefirst -e PUBLIC_KEY="$pubkey" -e USER_NAME=codefirst -e PUID=0 --hostname=nginx codefirst-nginx)
  sleep 2 # Wait to let the container start
  docker run -v codefirst-docdeployer-drone:/drone/src \
    -v codefirst-docdeployer-docs:/docs \
    --net=codefirst \
    -e DRONE_REPO_NAME=hello-world \
    -e DRONE_REPO_OWNER=octocat \
    -e DRONE_REPO=octocat/hello-world \
    -e DRONE_BRANCH=$branch \
    -e DRONE_COMMIT=7fd1a60b01f91b314f59955a4e4d4e80d8edf11d \
    --entrypoint '/bin/bash' \
    --workdir /drone/src \
    --rm codefirst-docdeployer \
    -c "sed -i 's/ssh/ssh -p 2222/g' /entrypoint.sh && sed -i 's/root@nginx/codefirst@nginx/g' /entrypoint.sh && $*"
  docker stop "$nginx_id"
  docker volume rm codefirst-docdeployer-drone codefirst-docdeployer-docs > /dev/null
}

check_on_deployed() {
  # Run the given command on the codefirst-nginx volume.
  docker run --rm -i -v codefirst-nginx:/usr/share/nginx busybox $*
}

cleanup() {
  # Remove the codefirst-nginx volume after a test.
  docker volume rm codefirst-nginx > /dev/null
}

assert_file_exists() {
  # Assert that the given file exists in the codefirst-nginx volume.
  if check_on_deployed test -f "$EXPECTED_DOC_BASE_PATH/$1"; then
    echo -e "$1 exists: \e[32mOK\e[0m"
  else
    echo -e "$1 does not exist: \e[31mFAILED\e[0m"
    if [[ "$VERBOSE" == 'true' ]]; then
      check_on_deployed find /usr/share/nginx
    fi
  fi
}

assert_file_contains() {
  # Assert that the given file contains the given string.
  if check_on_deployed grep -q $2 "$EXPECTED_DOC_BASE_PATH/$1"; then
    echo -e "$1 exists and matches content: \e[32mOK\e[0m"
  elif check_on_deployed test -f "$EXPECTED_DOC_BASE_PATH/$1"; then
    echo -e "$1 exists but does not match content: \e[31mFAILED\e[0m"
    if [[ "$VERBOSE" == 'true' ]]; then
      check_on_deployed cat "$EXPECTED_DOC_BASE_PATH/$1"
    fi
  else
    echo -e "$1 does not exist: \e[31mFAILED\e[0m"
    if [[ "$VERBOSE" == 'true' ]]; then
      check_on_deployed find /usr/share/nginx
    fi
  fi
}

docker build -t codefirst-docdeployer "$root_dir"
docker build -t codefirst-nginx "$root_dir/test/nginx"

test_doxygen() {
  run_image main 'doxygen -g > /dev/null && /entrypoint.sh -b main -l . -t doxygen -d sourcecode_documentation'
  assert_file_exists sourcecode_documentation/doxygen/index.html
  cleanup
}

test_swagger() {
  run_image main 'touch api.yaml && /entrypoint.sh --type swagger --loc api.yaml'
  assert_file_exists swagger/api.yaml
  assert_file_contains swagger/CLICKME.html '/swagger?url=/documentation/octocat/hello-world/swagger/api.yaml'
  cleanup
}

test_mdbook() {
  run_image master 'mkdir -p doc/src \
    && touch doc/book.toml \
    && touch doc/src/index.md \
    && echo "[Introduction](index.md)" > doc/src/SUMMARY.md \
    && /entrypoint.sh -l doc -t mdbook'
  assert_file_exists book/index.html
  cleanup
}

test_doxygen
test_swagger
test_mdbook
