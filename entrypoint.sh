#!/usr/bin/env bash

echo "Repository name : $DRONE_REPO_NAME"
echo "Repository owner: $DRONE_REPO_OWNER"

echo "Generating and deploying documentation for user $DRONE_REPO_OWNER and repository $DRONE_REPO_NAME"

help() {
  cat <<EOF
usage: $0
  -l --loc [dir] location of the documentation sources root - required
  -b --branch [branch] pattern of branch names to deploy - optional
  -t --type [docusaurus|doxygen|mdbook|swagger] type of documentation generator to use. - required
  -d --dest [dir] path to where to put the documentation outputs in your repository's CodeDoc space - optional !! cannot be absolute !!
EOF
  exit 1
}

BRANCH=""
DEST=""
while [ "$1" ]; do
  case "$1" in
  "-b" | "--branch")
    BRANCH="$2"
    shift 1
    ;;
  "-d" | "--dest")
    DEST="$2"
    shift 1
    ;;
  "-l" | "--loc")
    DOC_DIR="$2"
    shift 1
    ;;
  "-t" | "--type")
    GENERATOR_NAME="$2"
    GENERATOR_SCRIPT="/generators/$GENERATOR_NAME.sh"
    if [ ! -f "$GENERATOR_SCRIPT" ]; then
      echo "unknown generator type, please enter a valid generator ($(ls /generators | cut -d "." -f1 | tr "\n" " "))" >&2
      exit 1
    fi
    shift 1
    ;;
  *)
    echo "unknown option $1" >&2
    exit 1
    ;;
  esac
  shift
done

if [ ! "$GENERATOR_SCRIPT" ] || [ ! "$DOC_DIR" ] || echo "$DEST" | grep -E -q "^[\/].*"; then
  echo "$0: bad usage" >&2
  help
fi

RELATIVE_PATH=$(echo "$DRONE_REPO_OWNER/$DRONE_REPO_NAME/$DEST/" | tr -s "/")

if [[ -n $BRANCH && ! $DRONE_BRANCH =~ $BRANCH ]]; then
  echo "ignoring deploy step since current branch doesn't match $BRANCH"
  exit
fi

. "$GENERATOR_SCRIPT"
generate "$DOC_DIR" # generates doc using the wanted generator

SERVER_TARGET="/usr/share/nginx/html/$RELATIVE_PATH"

# launches rsync in archive, verbose and compression mode
# creates target directory ($SERVER_TARGET) on server
# then sends generated files into the server directory
rsync -avz -I \
  --rsync-path="mkdir -p \"$SERVER_TARGET\" && rsync" \
  -e "ssh -o StrictHostKeyChecking=no" \
  --delete "$GEN_PATH" root@nginx:"$SERVER_TARGET"

echo "documentation generated and deployed at https://codefirst.iut.uca.fr/documentation/${RELATIVE_PATH}index.html"
